I implemented a gomoku game based on AI algorithms. Gomoku, also called Five in a Row, is an abstract strategy board game. 
The winner of this game is the first player to form an unbroken chain of five stones horizontally, vertically, or diagonally. 
I use the game tree search, Minimax search tree and Alpha-beta pruning tree. Also, I use the negative maxima method and other methods for optimization.



How To Play

Put Gomoku.py and board.py in the same directory. Then you can run it. The interface is like this. 
You can play with tree search method in gomoku game. It is a 15 * 15 board.
To run the machine learning AI, run the human_play.py, to train the model, run train.py. 
It is a 11 * 11 board. Because I could not train it well in a 15 * 15 board, I choose a smaller board.











